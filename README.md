# ms-producto-rest

Demo microservicio rest. CRUD.


## Endpoints
Los endpoints que posee el aplicativo son los siguientes

### GET http://localhost:8001/api/v1.0/acl/producto/listar
### GET http://localhost:8001/api/v1.0/acl/producto/{ID}
### POST http://localhost:8001/api/v1.0/acl/producto/{ID}
request body:
```bash
{
	"nombre":"notebook lenovo gaming",
	"precio":900000,
	"createAt":"2019-10-10"
}
```
### PUT http://localhost:8001/api/v1.0/acl/producto/{ID}
request body:
```bash
{
	"id":7,
	"nombre":"notebook lenovo gaming",
	"precio":900000,
	"createAt":"2019-10-10"
}
```
### DELETE http://localhost:8001/api/v1.0/acl/producto/{ID}