package com.acl.producto.service;

import java.util.List;

import com.acl.producto.model.entity.Producto;

public interface ProductoService {
	public List<Producto> findAll();
	public Producto findById(Long id);
	public Boolean create(Producto producto);
	public Boolean update(Producto producto);
	public Boolean delete(Long id);
}
