package com.acl.producto.controller;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.acl.producto.model.entity.Producto;
import com.acl.producto.service.impl.ProductoServiceImpl;

@RestController
public class ProductoController {
	
	@Autowired
	private Environment env;
	
	@Autowired
	private ProductoServiceImpl productoServiceImpl;

	@GetMapping("/listar")
	public List<Producto> listar(){
		return productoServiceImpl.findAll().stream().map(
				p-> {p.setPort(Integer.parseInt(env.getProperty("local.server.port")));
					 return p;}).collect(Collectors.toList());
	}
	
	@GetMapping("/{id}")
	public Producto detalle(@PathVariable Long id){
		Producto producto = productoServiceImpl.findById(id);
		producto.setPort(Integer.parseInt(env.getProperty("local.server.port")));
		return producto;
	}
	
	@PostMapping("/")
	public boolean crearProducto(@RequestBody Producto producto){

		return productoServiceImpl.create(producto);
	}
	
	@PutMapping("/")
	public boolean ActualizarProducto(@RequestBody Producto producto) {

		return productoServiceImpl.update(producto);
	}
	
	@DeleteMapping("/{id}")
	public boolean EliminarProducto(@PathVariable Long id){

		return productoServiceImpl.delete(id);
	}
}
