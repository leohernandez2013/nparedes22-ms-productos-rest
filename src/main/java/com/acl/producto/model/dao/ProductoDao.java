package com.acl.producto.model.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;

import com.acl.producto.model.entity.Producto;

public interface ProductoDao extends CrudRepository<Producto, Long>{

}
